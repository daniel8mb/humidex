package app.com.humidex_2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import app.com.humidex_2.Addon.Pref;
import app.com.humidex_2.tools.Tools;

/**
 * Created by Daniel-PC on 10/21/2016.
 */

public class UnitSettingActivity extends Activity {

    private Spinner unitSpin;
    private Spinner humTargetSpin;
    private Spinner fanSpeed;

    private TextView targetHumText, unitNameText;
    private Button sumbitButton;
    private Button cancelButton;

    private String[] valueUnitSpinTab;
    /*
    * Valeurs qui seront envoyées à Firebase
    * */
    private String valueUnitSpin = "";
    private String valueHumTargetSpin = "";
    private int valueFanSpeed = 0;

    public static String unitDataKey;

    private DatabaseReference unitDataRef,userUnitDataRef;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.unit_settings);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //unitSpin = (Spinner) findViewById(R.id.unit_spin);
        humTargetSpin = (Spinner) findViewById(R.id.hum_target_spin);
        fanSpeed = (Spinner) findViewById(R.id.fan_speed);

        targetHumText = (TextView) findViewById(R.id.target_hum_text);
        unitNameText = (TextView) findViewById(R.id.unitNameText);
        sumbitButton = (Button) findViewById(R.id.submit_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);


        // get unit name from ListUnitActivity
        unitNameText.setText(ListUnitActivity.selectedUnitName);

        unitDataKey = getIntent().getStringExtra(Pref.UNITDATAKEY);
        int desiredHunidity = getIntent().getExtras().getInt(Pref.UNITDESIREDHUMIDITY);
        int maxFunSpeed =  getIntent().getExtras().getInt(Pref.UNITMAXFUNSPEED);
        // set selected speed
        fanSpeed.setSelection(maxFunSpeed-1);

        // set selected desired humidity
        String desireddHumidityStr = desiredHunidity+"%";
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.humidity_target_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        humTargetSpin.setAdapter(adapter);
        if (!desireddHumidityStr.equals(null)) {
            int spinnerPosition = adapter.getPosition(desireddHumidityStr);
            humTargetSpin.setSelection(spinnerPosition);
        }


        System.out.println("unitDataKey: "+unitDataKey);

        unitDataRef = FirebaseDatabase.getInstance().getReference("units").child(unitDataKey);
        userUnitDataRef = FirebaseDatabase.getInstance().getReference("users_units").child(LoginSignUpActivity.authUser.getUid()).child(unitDataKey);

        actionSpinner();
        actionButton();
    }

    /**
     * Actions des boutons submitButton et CancelButton
     * */
    public void actionButton(){
        sumbitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Tools.isNetworkAvailable(UnitSettingActivity.this)){
                    unitDataRef.child("config").child("fan_speeds").child("1").setValue(valueFanSpeed);
                    unitDataRef.child("config").child("desired_humidity").setValue(Integer.parseInt(valueHumTargetSpin.replace("%","")));
                    userUnitDataRef.child("name").setValue(unitNameText.getText().toString());
                    finish();
                }
                else
                {
                    Tools.alert("No network connection", "Your device is not connected, please check your network connection and try again.",UnitSettingActivity.this);
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     * Action des Spinners
     * Recupere les valeurs des spinners
     * */
    public void actionSpinner(){
        valueUnitSpinTab = new String[Pref.listUnitKey.size()];
        for (int i = 0; i < Pref.listUnitKey.size(); i++){
            valueUnitSpinTab[i] = Pref.listUnitKey.get(i).toString();
        }
        /*
        populateSpinner(unitSpin, valueUnitSpinTab);
        unitSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                valueUnitSpin = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        */

        humTargetSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try{
                    ((TextView) humTargetSpin.getSelectedView()).setTextColor(Color.BLUE);
                    ((TextView) humTargetSpin.getSelectedView()).setTextSize(16);
                }catch(Exception e){
                    e.printStackTrace();
                }
                valueHumTargetSpin = adapterView.getSelectedItem().toString();
                targetHumText.setText(valueHumTargetSpin);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fanSpeed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                /*
                * Convertit un mot en un entier
                * */

               try{
                   ((TextView) fanSpeed.getSelectedView()).setTextColor(Color.BLUE);
                   ((TextView) fanSpeed.getSelectedView()).setTextSize(16);
               }catch(Exception e){
                   e.printStackTrace();
               }

                switch (adapterView.getSelectedItem().toString()){
                    case "Low":
                        valueFanSpeed = 1;
                        break;
                    case "Medium":
                        valueFanSpeed = 2;
                        break;
                    case "Intermediate":
                        valueFanSpeed = 3;
                        break;
                    case "High":
                        valueFanSpeed = 4;
                        break;
                }

                setSpeedPic(valueFanSpeed);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    /**
     * Description: En fonction de la vitesse, affiche les images adequates
     * @Param int i : fan speeds
     * */
    public void setSpeedPic(int i){
        View sp1 = findViewById(R.id.speed1);
        View sp2 = findViewById(R.id.speed2);
        View sp3 = findViewById(R.id.speed3);
        View sp4 = findViewById(R.id.speed4);

        switch (i){
            case 1:
                sp1.setVisibility(View.VISIBLE);
                sp2.setVisibility(View.GONE);
                sp3.setVisibility(View.GONE);
                sp4.setVisibility(View.GONE);
                break;
            case 2:
                sp1.setVisibility(View.GONE);
                sp2.setVisibility(View.VISIBLE);
                sp3.setVisibility(View.GONE);
                sp4.setVisibility(View.GONE);
                break;
            case 3:
                sp1.setVisibility(View.GONE);
                sp2.setVisibility(View.GONE);
                sp3.setVisibility(View.VISIBLE);
                sp4.setVisibility(View.GONE);
                break;
            case 4:
                sp1.setVisibility(View.GONE);
                sp2.setVisibility(View.GONE);
                sp3.setVisibility(View.GONE);
                sp4.setVisibility(View.VISIBLE);
                break;

        }
    }

    public void populateSpinner(Spinner spinner, String[] prov_states){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, prov_states);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }
}
