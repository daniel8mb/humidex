package app.com.humidex_2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ArrayRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import app.com.humidex_2.Addon.ManagePref;
import app.com.humidex_2.Addon.Pref;
import app.com.humidex_2.Entity.ProfileEntity;
import app.com.humidex_2.Entity.User;
import app.com.humidex_2.tools.Tools;

/**
 * Created by Daniel-PC on 10/19/2016.
 *
 * Represent the ProfileActivity Activity
 * If the user exist, with the submit button, the user update his profil.
 * If the user don't exist, the submit button will create a new User in the Firebase Database
 */

public class ProfileActivity extends AppCompatActivity {

    public static boolean provinceUpdated = false;

    private EditText first_name;
    private EditText last_name;
    private EditText city;
    private EditText address;
    private EditText areaCode;
    private EditText phone_number;

    private Spinner countrySpin;
    private Spinner provSpin;

    private Button submitBtn;
    private Button cancelBtn;

    private String t_first_name = "";//text for first name
    private String t_last_name = "";//text for last name
    private String t_city = "";//text for city
    private String t_address = "";//text for address
    private String t_areaCode = "";//text for area code
    private String t_phone_number = "";//text for phone number
    private String t_country = "";//text for country
    private String t_prov = "";//text for provinces or State
    private String t_email = "";//text for email
    private String t_password = "";//text for password

    private DatabaseReference myRef;

    private FirebaseUser user;//Le User
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ManagePref managePref;

    private static final String TAG = "ProfileActivity";

    String[] provinces = {"Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Northwest Territories", "Nova Scotia", "Nunavut", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", "Yukon"};
    String[] states = {"Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi","Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah ", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"};

    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // init interface components
        initComponents();

        myRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        // get current firebase user
        user = mAuth.getCurrentUser();

        pref = getSharedPreferences(Pref.PREFS_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        t_email = getIntent().getStringExtra(LoginSignUpActivity.USER_EMAIL_EXTRA);
        t_password = getIntent().getStringExtra(LoginSignUpActivity.USER_PASS_EXTRA);

        managePref = new ManagePref(this.getApplicationContext());


        Intent intent = getIntent();
        boolean register = intent.getExtras().getInt("REGISTER_UPDATE")==1;
        if(register){
           // Tools.alert("Register", this);
        }
        else{
            //Tools.alert("Update", this);
            initPref();
            getUserFromStore();

        }



        initButtonAction();


    }


    /**
     * Init components
     */
    public void initComponents(){
        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        city = (EditText) findViewById(R.id.city);
        address = (EditText) findViewById(R.id.address);
        areaCode = (EditText) findViewById(R.id.area_code);
        phone_number = (EditText) findViewById(R.id.phone_number);

        countrySpin = (Spinner) findViewById(R.id.country_spin);
        provSpin = (Spinner) findViewById(R.id.prov_spin);

        submitBtn = (Button) findViewById(R.id.submit_button);
        cancelBtn = ( Button) findViewById(R.id.cancel_button);
        actionSpinner();
    }


    public void initButtonAction(){
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collectData();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    public void getFirebaseData(){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{

                }
                catch (Exception e){
                    Tools.alert(getResources().getString(R.string.error_getting_current_data_text), ProfileActivity.this);
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Collect data from editView and Spinner
     * Put data in the Firebase Database
     * Put data in Preferences
     * */

    public void collectData(){
        t_address = address.getText().toString();
        t_areaCode = areaCode.getText().toString();
        t_phone_number = phone_number.getText().toString();
        t_city = city.getText().toString();
        t_first_name = first_name.getText().toString();
        t_last_name = last_name.getText().toString();

        editor.putString(t_address, Pref.PREF_ADRRESS);
        editor.putString(t_areaCode, Pref.PREF_AREA_CODE);
        editor.putString(t_phone_number, Pref.PREF_PHONE_NUMBER);
        editor.putString(t_city, Pref.PREF_CITY);
        editor.putString(t_first_name, Pref.PREF_FIRST_NAME);
        editor.putString(t_last_name, Pref.PREF_LAST_NAME);
        editor.commit();

        User entityUser = new User(t_address, t_city, t_country, t_areaCode, t_email, t_first_name, t_last_name, t_prov, t_phone_number);
        if (user != null){
            System.out.println("USER FOUND");
            sendDataToFirebase(entityUser);
            Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            System.out.println("REGISTER");
            register(entityUser);
        }

    }

    public void getUserFromStore(){
        User mUser = managePref.getPersoUserFromPref();
        if (mUser != null){
            address.setText(mUser.getAddress());
            areaCode.setText(mUser.getZip_code());
            phone_number.setText(mUser.getPhone_number());
            city.setText(mUser.getCity());
            first_name.setText(mUser.getFirst_name());
            last_name.setText(mUser.getLast_name());

            String country = mUser.getCountry();



        }

        else {
            myRef.child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                        try{
                            String zip = dataSnapshot.child("zip_code").getValue().toString();
                            areaCode.setText(zip);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                            areaCode.setText("");
                            myRef.child("users").child(user.getUid()).child("zip_code").setValue("");
                        }

                    try{
                        address.setText(dataSnapshot.child("address").getValue().toString());

                        phone_number.setText(dataSnapshot.child("phone_number").getValue().toString());
                        city.setText(dataSnapshot.child("city").getValue().toString());
                        first_name.setText(dataSnapshot.child("first_name").getValue().toString());
                        last_name.setText(dataSnapshot.child("last_name").getValue().toString());
                        t_prov = dataSnapshot.child("province").getValue().toString();
                        t_country = dataSnapshot.child("country").getValue().toString();



                        setSpinnerSelectedItem(countrySpin, t_country, R.array.country_arrays);
                        if(t_country.equals("Canada")) {
                            setSpinnerSelectedItem(provSpin, t_prov, R.array.province_arrays);
                            provinceUpdated = true;
                        }
                        else if(t_country.equals("United States"))
                        {
                            setSpinnerSelectedItem(provSpin, t_prov, R.array.state_arrays);
                            provinceUpdated = true;
                        }



                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    public void setSpinnerSelectedItem(Spinner spinner, String item, int resId){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(ProfileActivity.this, resId, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (!item.equals(null)) {
            int spinnerPosition = adapter.getPosition(item);
            System.out.println("Selected index:"+spinnerPosition);
            spinner.setSelection(spinnerPosition);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    t_prov = adapterView.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    /**
     * Create new User
     * @Param User mUserEntity : entite personnalisée du User.
     * Sera envoyé à Firebase
     * */
    public void register(final User mUserEntity){
        mUserEntity.setUser_id(user.getUid());
        sendDataToFirebase(mUserEntity);
        managePref.savePersoUserToPref(mUserEntity);
        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Envoi des data à Firebase
     * @Param User mUserEntity : entite personnalisée du User.
     * Sera envoyé à Firebase
     * */
    public void sendDataToFirebase(User mUserEntity){
        myRef.child("users").child(user.getUid()).setValue(mUserEntity);
        myRef.child("users_units").child(user.getUid());
    }

    /**
     * Own action for the Spinners.
     * After country is selected, provSpin is updated.
     * */
    public void actionSpinner(){

        countrySpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                t_country = adapterView.getSelectedItem().toString();

                if (t_country.equals("Canada")){
                    populateSpinner(provSpin, provinces);
                }
                else if (t_country.equals("United States")){
                    populateSpinner(provSpin, states);
                }
                else{
                    t_prov = "";
                }

            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void populateSpinner(Spinner spinner, String[] prov_states){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, prov_states);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }



    /**
     * Recupere les preferences et les affiches sur les vues appropriées
     * */

    public void initPref(){
        first_name.setText(pref.getString(Pref.PREF_FIRST_NAME, ""));
        last_name.setText(pref.getString(Pref.PREF_LAST_NAME, ""));
        phone_number.setText(pref.getString(Pref.PREF_PHONE_NUMBER, ""));
        city.setText(pref.getString(Pref.PREF_CITY, ""));
        address.setText(pref.getString(Pref.PREF_ADRRESS, ""));
        areaCode.setText(pref.getString(Pref.PREF_AREA_CODE, ""));
    }
}
