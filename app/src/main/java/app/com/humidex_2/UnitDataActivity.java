package app.com.humidex_2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Dimension;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import app.com.humidex_2.Addon.Pref;
import app.com.humidex_2.Entity.UnitData;
import app.com.humidex_2.tools.CastDate;
import app.com.humidex_2.tools.Tools;

/**
 * Created by Daniel-PC on 10/22/2016.
 */
public class UnitDataActivity extends Activity{


    private ViewFlipper viewFlipper;
    private TextView hourText;//Heure locale
    private TextView temp_text;//Temperature interireur et exterieur
    private TextView prt_text;//Pourcentage d'humiditÃ©
    private TextView unit_name;
    private TextView toDay;
    private TextView humidityText;
    private TextView dewPointText, dewValueLabel, humidityValueLabel;
    private TextView tempText;

    private LinearLayout mainLayout;

    long config_desired_humidity,config_fan_speeds;

    private TextView temp_text_ex;//Temperature interireur et exterieur
    /**
     * Vue Ã  afficher en fonction de la vitesse
     * */
    private View sp1;
    private View sp2;
    private View sp3;
    private View sp4;

    private TextView timeUpdate;

    private LinearLayout settings;

    private DatabaseReference myRef;

    private FirebaseUser user;//Le User
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private float lastX;

    private String id_unit = "";//Id de l'Unite

    private static final String TAG = "EmailPassword";
    public static List<UnitData> listContent = null;

    private Intent intent;
    private UnitData unitData;
    private static String key = "";
    private String unitId = "";
    private CastDate castDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_unit_data);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listContent = new ArrayList<UnitData>();

        mainLayout = (LinearLayout) findViewById(R.id.main_layout);

        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper1);

        viewFlipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent touchevent) {
                if(v.getId()==R.id.viewFlipper1){
                    System.out.println("TOUCH EVENT");
                    Rect editTextRect = new Rect();
                    viewFlipper.getHitRect(editTextRect);

                        System.out.println("FIRST IF:"+touchevent.getAction()+"\tMotionEvent.ACTION_DOWN:"+MotionEvent.ACTION_DOWN+"\tMotionEvent.ACTION_UP:"+MotionEvent.ACTION_UP);
                        switch (touchevent.getAction()) {

                            case MotionEvent.ACTION_DOWN:
                                System.out.println("ACTION_DOWN");
                                lastX = touchevent.getX();
                                break;
                            case MotionEvent.ACTION_UP:
                                System.out.println("ACTION_UP");
                                float currentX = touchevent.getX();

                                // Handling left to right screen swap.
                                if (lastX < currentX) {

                                    // If there aren't any other children, just break.
                                    if (viewFlipper.getDisplayedChild() == 0)
                                        break;

                                    // Next screen comes in from left.

                                    viewFlipper.setInAnimation(UnitDataActivity.this, R.anim.slide_in_from_left);
                                    // Current screen goes out from right.
                                    viewFlipper.setOutAnimation(UnitDataActivity.this, R.anim.slide_out_to_right);

                                    // Display next screen.
                                    viewFlipper.showNext();
                                }

                                // Handling right to left screen swap.
                                if (lastX > currentX) {

                                    // If there is a child (to the left), kust break.
                                    if (viewFlipper.getDisplayedChild() == 1)
                                        break;

                                    // Next screen comes in from right.
                                    viewFlipper.setInAnimation(UnitDataActivity.this, R.anim.slide_in_from_right);
                                    // Current screen goes out from left.
                                    viewFlipper.setOutAnimation(UnitDataActivity.this, R.anim.slide_out_to_left);

                                    // Display previous screen.
                                    viewFlipper.showPrevious();
                                }
                                break;
                        }
                    return true;
                }
                return false;
            }
        });

        sp1 = findViewById(R.id.main_speed1);
        sp2 = findViewById(R.id.main_speed2);
        sp3 = findViewById(R.id.main_speed3);
        sp4 = findViewById(R.id.main_speed4);

        timeUpdate = (TextView) findViewById(R.id.time_update);

        settings = (LinearLayout) findViewById(R.id.settings);

        hourText = (TextView) findViewById(R.id.time);
        tempText = (TextView) findViewById(R.id.temp_firebase);
        humidityText = (TextView) findViewById(R.id.humidity_firebase);
        dewPointText = (TextView) findViewById(R.id.dew_firebase);
        humidityValueLabel = (TextView) findViewById(R.id.humidityValue);
        dewValueLabel = (TextView) findViewById(R.id.dewValue);

        temp_text = (TextView) findViewById(R.id.temp_text);
        prt_text = (TextView) findViewById(R.id.target_text);
        toDay = (TextView) findViewById(R.id.date);

        temp_text_ex = (TextView) findViewById(R.id.temp_text_ex);
        unit_name = (TextView) findViewById(R.id.unit_name);

        //myRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        castDate = new CastDate();
        toDay.setText(castDate.getToday());

        intent = getIntent();
        if(intent!=null){
            unit_name.setText(ListUnitActivity.selectedUnitName);
            key = intent.getStringExtra(Pref.UNITDATA);
            intent = null;
        }
        else
        {
            key = UnitSettingActivity.unitDataKey;
        }


        setScreen();
        initUser();
        getData();
        btnAction(key);
    }
    /**
     * Set size of the first view. Force it to have the size's screen
     * */
    public void setScreen(){

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        //firstPart.getLayoutParams().width = width;
        //firstPart.getLayoutParams().height = height;
    }


    /**
     * Recupere les donnees de FireBase
     * */

    public void getData(){
        if (key != null){
            myRef = FirebaseDatabase.getInstance().getReference("units").child(key);
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try{
                        config_desired_humidity = (long) dataSnapshot.child("config").child("desired_humidity").getValue();
                        config_fan_speeds = (long)  dataSnapshot.child("config").child("fan_speeds").child("1").getValue();

                        prt_text.setText(config_desired_humidity + "%");
                        setSpeedPic((int)config_fan_speeds);


                        DataSnapshot dataSnapshot1 = dataSnapshot.child("currentdata");

                        String exhaust_inside_temp = dataSnapshot1.child("exhaust").child("inside").child("temperature").getValue().toString();
                        String exhaust_outside_temp = dataSnapshot1.child("exhaust").child("outside").child("temperature").getValue().toString();

                        String supply_inside_temp = dataSnapshot1.child("supply").child("inside").child("temperature").getValue().toString();
                        String supply_outside_temp = dataSnapshot1.child("supply").child("outside").child("temperature").getValue().toString();

                        String weather_temp = dataSnapshot1.child("weather").child("temperature").getValue().toString() + " °C";
                        String weather_humidity = dataSnapshot1.child("weather").child("humidity").getValue().toString() + " %";
                        String dewpointValue = dataSnapshot1.child("weather").child("dewpoint").getValue().toString() + " °C";


                        tempText.setText(weather_temp);//Ajout d'humidité
                        humidityValueLabel.setText(weather_humidity);//Ajout temperature
                        dewValueLabel.setText(dewpointValue);

                        String timeStamp_from_firebase = dataSnapshot1.child("timestamp").getValue().toString();

                        temp_text.setText(supply_inside_temp + "°C | " + supply_outside_temp+ "°C");
                        temp_text_ex.setText(exhaust_inside_temp +  "°C | " + exhaust_outside_temp + "°C");

                        displayUpdateTime(timeStamp_from_firebase);
                    }
                    catch (Exception e){

                        AlertDialog.Builder builder = new AlertDialog.Builder(UnitDataActivity.this);
                        builder.setTitle("Error");
                        builder.setMessage(getResources().getString(R.string.error_getting_current_data_text)+".\nNo data found.")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                       finish();
                                    }
                                });

                        // Create the AlertDialog object
                        builder.create();
                        builder.show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void alert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });

        // Create the AlertDialog object
        builder.create();
        builder.show();
    }

    /**
     * @Description : Recuperer la timestamp de Firebase, la cast et l'affiche dans le textView appropriÃ©
     * */

    public void displayUpdateTime(String timeStamp_from_firebase){
        castDate.getTimeUpdateFromFirebase(timeStamp_from_firebase);
        String hour = castDate.getHour();
        String min = castDate.getMin();
        String sec = castDate.getSec();
        String year = castDate.getYear();
        String month = castDate.getMonth();
        String day = castDate.getDay();

        String textUpdateTime = getResources().getString(R.string.timeUpdateText) + " " + month + "-"
                + day + "-" + year +" "+ getResources().getString(R.string.at) + " " + hour + ":" + min;
        System.out.print("textUpdateTime: "+textUpdateTime);

        timeUpdate.setText(textUpdateTime);
    }

    /**
     * @Description : Action sur les layouts.
     * Ici, settings. Un appui permet de se rendre sur UnitSetting.
     * @Param String unitId : id de l'unitÃ©. Celle ci sera envoyÃ© en extra Ã  UnitSetting.
     * */
    public void btnAction(final String unitId){

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Tools.isNetworkAvailable(UnitDataActivity.this)){
                    Intent intent = new Intent(UnitDataActivity.this, UnitSettingActivity.class);
                    intent.putExtra(Pref.UNITDATAKEY, unitId);
                    intent.putExtra(Pref.UNITDESIREDHUMIDITY, (int)config_desired_humidity);
                    intent.putExtra(Pref.UNITMAXFUNSPEED, (int)config_fan_speeds);

                    startActivity(intent);
                }
                else
                {
                    Tools.alert("No network connection", "Your device is not connected, please check your network connection and try again.",UnitDataActivity.this);
                }

            }
        });
    }

    /**
     * Initialise le User. S'assure qu il est connectÃ©
     * */

    public void initUser(){

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }

    /**
     * Recupere les donnees de FireBase
     * */

    public void initFBase(){
        final DatabaseReference refUnits = myRef.child("units");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String userId = user.getUid();
                List listKey = new ArrayList();
                DataSnapshot snapUnit = dataSnapshot.child("users_units");
                if (snapUnit.child(userId).exists()){
                    for (DataSnapshot dataSnapshot1 : snapUnit.child(userId).getChildren()){
                        listKey.add(dataSnapshot1.getKey());
                    }

                    for (int i = 0; i < listKey.size(); i++){
                       /*
                        DataSnapshot dataUnits = dataSnapshot.child("units").child((String) listKey.get(i));
                        UnitData unitData = new UnitData(dataUnits);
                        unitData.setIdUnit((String) listKey.get(i));
                        listContent.add(unitData);
                        //Log.d("TAGGGA", (String) listKey.get(i));
                        */

                    }

                    UnitData unitData = listContent.get(0);
                    temp_text.setText(unitData.getTempSupplyInside() + " C | " + unitData.getTempSupplyOuside() + " C");
                    temp_text_ex.setText(unitData.getTempExhaustInside() +  " C | " + unitData.getTempExhaustOutside() + "C");

                    for (int i = 0; i < listContent.size(); i++){
                        Log.d("Tag", listContent.get(i).getIdUnit());
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * @brief Description
     * @Param int fan_speed : vitesse d'helice
     * @return void
     * */

    public void setSpeedPic(int fan_speed){

        switch (fan_speed){
            case 1:
                sp1.setVisibility(View.VISIBLE);
                sp2.setVisibility(View.GONE);
                sp3.setVisibility(View.GONE);
                sp4.setVisibility(View.GONE);
                break;
            case 2:
                sp1.setVisibility(View.GONE);
                sp2.setVisibility(View.VISIBLE);
                sp3.setVisibility(View.GONE);
                sp4.setVisibility(View.GONE);
                break;
            case 3:
                sp1.setVisibility(View.GONE);
                sp2.setVisibility(View.GONE);
                sp3.setVisibility(View.VISIBLE);
                sp4.setVisibility(View.GONE);
                break;
            case 4:
                sp1.setVisibility(View.GONE);
                sp2.setVisibility(View.GONE);
                sp3.setVisibility(View.GONE);
                sp4.setVisibility(View.VISIBLE);
                break;

        }
    }



}