package app.com.humidex_2.Addon;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import app.com.humidex_2.Entity.User;

/**
 * Created by Daniel-PC on 10/27/2016.
 */

public class ManagePref {

    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    public ManagePref(Context context){

        pref = context.getSharedPreferences(Pref.PREFS_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }


    /**
     * @Description : Save the User entity in the SharedPreferences
     * @Param User : myPersonUser
     * @return boolean
     * */
    public boolean savePersoUserToPref(User myPersoUser){

        Gson gson = new Gson();
        String json = gson.toJson(myPersoUser);
        editor.putString(Pref.PREF_MY_USER, json);
        editor.commit();

        return true;
    }

    /**
     * @Description: Get User from the SharedPreferences
     * */
    public User getPersoUserFromPref(){

        Gson gson = new Gson();
        String json = pref.getString(Pref.PREF_MY_USER, "");
        User obj = gson.fromJson(json, User.class);

        return obj;
    }
}
