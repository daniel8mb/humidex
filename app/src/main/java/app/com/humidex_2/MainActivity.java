package app.com.humidex_2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.android.volley.Request;
//import com.android.volley.toolbox.StringRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import app.com.humidex_2.Addon.Pref;
import app.com.humidex_2.Entity.AuthUser;
import app.com.humidex_2.Entity.UnitData;
import app.com.humidex_2.tools.Tools;

public class MainActivity extends Activity {

    public static String NBR_UNITS = "NBR_UNITS";

    ProgressDialog loadingProgress;

    public static AuthUser authUser;
    static long nbrUnits;
    private DatabaseReference myRef;
    static String userId;
    private FirebaseUser user;//Le User
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private LinearLayout addUnit;
    private LinearLayout viewUnitList;
    private LinearLayout profile;
    private LinearLayout logout;

    TextView unitsLabel, nbrUnitsLabel;


    public static ArrayList<UnitData> listUnitData;
    private List listKey;
    public static final String TAG = "MainActivity.TAG";

    public TextView mainTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_c);



        addUnit = (LinearLayout) findViewById(R.id.main_add_unit);
        viewUnitList = (LinearLayout) findViewById(R.id.main_view_list_unit);
        profile = (LinearLayout) findViewById(R.id.profile);
        logout = (LinearLayout) findViewById(R.id.logout);

        unitsLabel = (TextView) findViewById(R.id.unitsLabel);
        nbrUnitsLabel = (TextView) findViewById(R.id.nbrUnitsLabel);
        mainTitle = (TextView) findViewById(R.id.main_title);

        //myRef = FirebaseDatabase.getInstance().getReference("users_units");
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        listUnitData = new ArrayList<UnitData>();
        listKey = new ArrayList();
        System.out.println("MainActivity onCreate()");

        // Scan current user units
        scanUserUnits();
        btnAction();

        if(!Tools.isNetworkAvailable(this)){
            nbrUnitsLabel.setText("No network connection");
            Tools.alert("No network connection", "Your device is not connected, please check your network connection and try again.",this);
        }



    }






    public void refreshData(){
        loadingProgress = new ProgressDialog(this);
        loadingProgress.setMessage("Loading user data, please wait...");
        loadingProgress.show();
        // Execute some code after 4 seconds have passed. This used to wait until the device get connected to be able to get the host IP address
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(loadingProgress!=null)
                    loadingProgress.dismiss();

                btnAction();
            }
        }, 2000);
    }

    public void btnAction(){

        addUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddUnitActivity.class);
                intent.putExtra(NBR_UNITS, nbrUnits);
                startActivity(intent);
            }
        });

        viewUnitList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Tools.isNetworkAvailable(MainActivity.this))
                    Tools.alert("No network connection", "Your device is not connected, please check your network connection and try again.",MainActivity.this);
                else{
                    Intent intent = new Intent(MainActivity.this, ListUnitActivity.class);
                    intent.putExtra(Pref.LISTDATA, listUnitData);
                    startActivity(intent);
                }

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Message");
                builder.setMessage("Are you sure you want to logout?")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                return;
                            }
                        });
                builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // sign out
                        mAuth.signOut();
                        startActivity(new Intent(MainActivity.this, LoginSignUpActivity.class));
                        finish();
                    }
                });
                // Create the AlertDialog object
                builder.create();
                builder.show();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Tools.isNetworkAvailable(MainActivity.this))
                    Tools.alert("No network connection", "Your device is not connected, please check your network connection and try again.",MainActivity.this);
                else {
                    Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                    intent.putExtra("REGISTER_UPDATE", 0);
                    startActivity(intent);
                }
            }
        });

    }

    /**
     * Initialise le User. S'assure qu il est connecté
     * */

    public void initUser(){

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    authUser = new AuthUser(user.getEmail(), user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }





    public void scanUserUnits(){
        System.out.println("Scanning current user units:  EMAIL: "+ LoginSignUpActivity.authUser.getEmail()+" ID: "+ LoginSignUpActivity.authUser.getUid());

        // create a reference to Firebase database
        DatabaseReference myDBRootRef = FirebaseDatabase.getInstance().getReference();
        // create a reference to Firebase database 'users' table
        DatabaseReference myUsersUnitsRef = myDBRootRef.child("users_units").getRef();
        DatabaseReference myUserssRef = myDBRootRef.child("users").getRef();

        myUserssRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    String firstName = dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).child("first_name").getValue().toString();
                    mainTitle.setText("Welcome "+firstName);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        myUsersUnitsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // print number of records in 'users' table
                Log.e("Count " ,""+dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).getChildrenCount());
                nbrUnits = dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).getChildrenCount();
                if(nbrUnits==1)
                    nbrUnitsLabel.setText(nbrUnits+" unit found");
                else if(nbrUnits>1)
                    nbrUnitsLabel.setText(nbrUnits+" units found");
                else
                    nbrUnitsLabel.setText("No unit found");
                for (DataSnapshot dataSnapshot1 : dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).getChildren()){
                    listKey.add(dataSnapshot1.getKey());
                    System.out.println("Unit id: "+dataSnapshot1.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


}
