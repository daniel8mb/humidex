package app.com.humidex_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import app.com.humidex_2.Addon.Pref;

/**
 * Created by Daniel-PC on 11/17/2016.
 * On se sert ici d un ViewPager pour afficher les
 * GraphDataFragment.
 * Chaque GraphDataFragment contient 3 PieChart correspondant
 * à l'humidité, la temperature et le DwePoint
 */

public class GraphDataActivity extends AppCompatActivity {

        public static String ARG_UNIT_ID = "GraphDataActivity.Unit_id";
        public static String ARG_UNIT_TYPE = "GraphDataActivity.Unit_type";

        private ViewPager viewPager;
        private PagerAdapter mPagerAdapter;

    private Intent intent;
    private static String key = "";

        @Override
        public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grap_data_activity_layout);

        viewPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mPagerAdapter);

        getSupportActionBar().setHomeButtonEnabled(true);

            intent = getIntent();
            if(intent!=null){
                //On recupere la cle de l unite
                key = intent.getStringExtra(Pref.UNITDATA);
                intent = null;
            }
            else
            {
                key = UnitSettingActivity.unitDataKey;
            }
        }


        /**
         * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
         * sequence.
         */
        private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
            public ScreenSlidePagerAdapter(FragmentManager fm) {
                super(fm);
            }

            @Override
            public Fragment getItem(int position) {
                /**
                 * On affiche le bon GraphDataFragment en fonction de la position, de la clé de l'unité
                 * et du type de données à affciher (weather, Supply, Exhaust)
                 * */
                switch (position){
                    case 0:
                        GraphDataFragment weatherDataFragment = new GraphDataFragment();
                        Bundle argsWeather = new Bundle();
                        //argsWeather.putString(ARG_UNIT_ID, "-KV6mz9xCYvnZiJ11Nsv");
                        argsWeather.putString(ARG_UNIT_ID, key);
                        argsWeather.putString(ARG_UNIT_TYPE, "weather");
                        weatherDataFragment.setArguments(argsWeather);

                        return weatherDataFragment;

                    case 1:
                        GraphDataFragment intakeDataFragment = new GraphDataFragment();
                        Bundle argsIntake = new Bundle();
                        //argsIntake.putString(ARG_UNIT_ID, "-KV6mz9xCYvnZiJ11Nsv");
                        argsIntake.putString(ARG_UNIT_ID, key);
                        argsIntake.putString(ARG_UNIT_TYPE, "supply");
                        intakeDataFragment.setArguments(argsIntake);

                        return intakeDataFragment;

                    case 2:
                        GraphDataFragment outputDataFragment = new GraphDataFragment();
                        Bundle argsOutput = new Bundle();
                        //argsOutput.putString(ARG_UNIT_ID, "-KV6mz9xCYvnZiJ11Nsv");
                        argsOutput.putString(ARG_UNIT_ID, key);
                        argsOutput.putString(ARG_UNIT_TYPE, "exhaust");
                        outputDataFragment.setArguments(argsOutput);

                        return outputDataFragment;

                    default:
                        return null;

                }
            }

            @Override
            public int getCount() {
                return 3;
            }
        }
}
