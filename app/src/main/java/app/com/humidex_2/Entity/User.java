package app.com.humidex_2.Entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Daniel-PC on 10/22/2016.
 */

public class User implements Parcelable {
    private String user_id = "";
    private String address = "";
    private String city = "";
    private String country = "";
    private String creation_date = "";
    private String email = "";
    private String first_name = "";
    private String last_name = "";
    private String province = "";
    private String phone_number = "";
    private String zip_code = "";

    public User(String address, String city, String country, String zip_code, String email, String first_name, String last_name, String province, String phone_number){

        this.address = address;
        this.city = city;
        this.country = country;
        this.zip_code = zip_code;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.province = province;
        this.phone_number = phone_number;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(creation_date);
        dest.writeString(email);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(province);
        dest.writeString(user_id);
    }

    public User(Parcel in){
        user_id = in.readString();
        address = in.readString();
        city = in.readString();
        country = in.readString();
        creation_date = in.readString();
        email = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        province = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>(){
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }

    };

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }
}
