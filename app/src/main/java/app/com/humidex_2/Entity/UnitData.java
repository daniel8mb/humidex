package app.com.humidex_2.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by Daniel-PC on 10/21/2016.
 */

public class UnitData implements Parcelable {

    private String idUnit;
    private String tempExhaustInside;
    private String tempExhaustOutside;
    private String tempSupplyInside;
    private String tempSupplyOuside;
    private String timestamp;

    public UnitData(){
        idUnit = "";
        tempExhaustInside = "";
        tempExhaustOutside = "";
        tempSupplyInside = "";
        tempSupplyOuside = "";
        timestamp = "";
    }

    public UnitData(String idUnit1, String tempExhaustInside1, String tempExhaustOutside1, String tempSupplyInside1, String tempSupplyOuside1){
        idUnit = idUnit1;
        tempExhaustInside = tempExhaustInside1;
        tempExhaustOutside = tempExhaustOutside1;
        tempSupplyInside = tempSupplyInside1;
        tempSupplyOuside = tempSupplyOuside1;
        timestamp = timestamp;
    }
/*
    public UnitData(DataSnapshot dataSnapshot){
        tempSupplyInside = dataSnapshot.child("current-data").child("supply").child("inside").child("temperature").getValue().toString();
        tempSupplyOuside = dataSnapshot.child("current-data").child("supply").child("outside").child("temperature").getValue().toString();
        tempExhaustOutside = dataSnapshot.child("current-data").child("exhaust").child("outside").child("temperature").getValue().toString();
        tempExhaustInside = dataSnapshot.child("current-data").child("exhaust").child("inside").child("temperature").getValue().toString();
        timestamp = dataSnapshot.child("current-data").child("timestamp").getValue().toString();
    }
*/
    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(idUnit);
        dest.writeString(tempExhaustInside);
        dest.writeString(tempExhaustOutside);
        dest.writeString(tempSupplyInside);
        dest.writeString(tempSupplyOuside);
        dest.writeString(timestamp);
    }

    public static final Parcelable.Creator<UnitData> CREATOR = new Parcelable.Creator<UnitData>(){
        @Override
        public UnitData createFromParcel(Parcel source) {
            return new UnitData(source);
        }

        @Override
        public UnitData[] newArray(int size) {
            return new UnitData[size];
        }

    };

    public UnitData(Parcel in){
        idUnit = in.readString();
        tempExhaustInside = in.readString();
        tempExhaustOutside = in.readString();
        tempSupplyInside = in.readString();
        tempExhaustOutside = in.readString();
        timestamp = in.readString();
    }

    public String getIdUnit() {
        return idUnit;
    }

    public void setIdUnit(String idUnit) {
        this.idUnit = idUnit;
    }

    public String getTempExhaustInside() {
        return tempExhaustInside;
    }

    public void setTempExhaustInside(String tempExhaustInside) {
        this.tempExhaustInside = tempExhaustInside;
    }

    public String getTempExhaustOutside() {
        return tempExhaustOutside;
    }

    public void setTempExhaustOutside(String tempExhaustOutside) {
        this.tempExhaustOutside = tempExhaustOutside;
    }

    public String getTempSupplyInside() {
        return tempSupplyInside;
    }

    public void setTempSupplyInside(String tempSupplyInside) {
        this.tempSupplyInside = tempSupplyInside;
    }

    public String getTempSupplyOuside() {
        return tempSupplyOuside;
    }

    public void setTempSupplyOuside(String tempSupplyOuside) {
        this.tempSupplyOuside = tempSupplyOuside;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
