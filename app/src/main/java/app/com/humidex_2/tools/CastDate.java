package app.com.humidex_2.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Daniel-PC on 10/18/2016.
 */

public class CastDate {
    private String hour = "";
    private String min = "";
    private String sec = "";
    private String year = "";
    private String month = "";
    private String day = "";

    public final String[] months = {"Jan.", "Feb.", "Mar.", "Apr.", "Mai.", "Jun.", "Jul.", "Aug.","Sep.", "Oct.", "Nov.", "Dec."};

    public CastDate(){

    }

    public CastDate(String timeStamp){
        try{
            long timestampLong = Long.parseLong(timeStamp)*1000;
            Date d = new Date(timestampLong);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            hour = "" + c.get(Calendar.HOUR);
            min = "" + c.get(Calendar.MINUTE);
            sec = "" + c.get(Calendar.SECOND);
            year = "" + c.get(Calendar.YEAR);
            month = "" + c.get(Calendar.MONTH);
            day = "" + c.get(Calendar.DAY_OF_WEEK);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getToday(){

        long dateLong = System.currentTimeMillis();
        Date d = new Date(dateLong);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        String mhour = "" + c.get(Calendar.HOUR);
        String mSec = "" + c.get(Calendar.SECOND);
        String mYear = "" + c.get(Calendar.YEAR);
        String mMonth = "" + c.get(Calendar.MONTH);
        String mDay = "" + c.get(Calendar.DAY_OF_MONTH);

        String monthName = months[c.get(Calendar.MONTH)];

        String weekDay;
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

        weekDay = dayFormat.format(c.getTime());

        String finalDate = weekDay+" "+monthName + " " + mDay + ", " + mYear;

        return finalDate;
    }

    public void getTimeUpdateFromFirebase(String timestamp){
        String[] myTimeStatmp = timestamp.split("_");
        year = myTimeStatmp[0];
        month = myTimeStatmp[1];
        day = myTimeStatmp[2];
        int mHourToParse = Integer.parseInt(myTimeStatmp[3]);
        int mDayToParse = Integer.parseInt(myTimeStatmp[2]);
        int mMonthToParse = Integer.parseInt(myTimeStatmp[1]);
        int mYearToParse = Integer.parseInt(myTimeStatmp[0]);
        int mHour = mHourToParse - 4;
        int mDay = 0;
        int mMonth = 0;
        int mYear = 0;

        if (mHourToParse == 3)
            mHour = 23;
        if (mHourToParse == 2)
            mHour = 22;
        if (mHourToParse == 1)
            mHour = 21;
        if (mHourToParse == 0)
            mHour = 20;

        if (mHourToParse < 4){

            mDay = mDayToParse - 1;
            if (mDayToParse == 1){
                mMonth = mMonthToParse - 1;
                if (mMonthToParse <= 7 && (mMonthToParse % 2) == 0){
                    mDay = 30;
                }
                else if (mMonthToParse == 2){
                    if (mYearToParse % 4 == 0){
                        mDay = 29;
                    }
                    else {
                        mDay = 28;
                    }
                }
                else {
                    mDay = 31;
                }

                if (mMonthToParse >= 8 && mMonthToParse <= 12 && (mMonthToParse % 2) == 1){
                    mDay = 30;
                }

                if (mMonthToParse == 1){
                    mMonth = 12;
                    mYear = mYearToParse - 1;
                }
            }
        }
        else {
            mDay = mDayToParse;
            mMonth = mMonthToParse;
            mYear = mYearToParse;
        }
        day = "" + mDay;
        month = "" + mMonth;
        year = "" + mYear;
        hour = "" + mHour;
        min = myTimeStatmp[4];
        sec = myTimeStatmp[5];

    }

    public String getOth2(String timestamp){
        String formattedDate = "";
        try{
            String dateStr = timestamp;
            SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(dateStr);
            df.setTimeZone(TimeZone.getDefault());
            formattedDate = df.format(date);
        }
        catch (Exception e){
            e.printStackTrace();
        }


        return formattedDate;
    }

    public String getOtherTime(String timestamp){

        String time = "";
        try {
            DateFormat utcFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse(timestamp);

            DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));

            time = pstFormat.format(date);
        }
        catch (ParseException e){
            e.printStackTrace();
        }

        return time;
    }
}