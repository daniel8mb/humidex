package app.com.humidex_2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import app.com.humidex_2.tools.Tools;

/**
 * Created by Daniel-PC on 10/19/2016.
 */

public class AddUnitActivity extends Activity implements AdapterView.OnItemSelectedListener{

    boolean reconnected = false;

    private Spinner unitsSpinner, homeNetworkSpinner;
    private EditText editPassword;

    private Button submitButton, cancelButton;
    ImageButton refreshButton;

    public String homeWifiSSID, homePassword;

    public static long currentNbrUnits;

    EditText passwordText;
    TextView messageLabel, homeNetworkLabel, unitLabel, detectUnitsLabel, detectWifisLabel;

    WifiManager mainWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();
    String wifis[];

    ProgressDialog loadingProgress;
    String filterText = "Humidex";
    String defaultPassword = "myhumidex"; //"12345678"; //
    private static int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION;
    private static String TAG = "MyWifiModule";

    private boolean deviceConnected = false;

    Handler handler = new Handler();

    List<ScanResult> wifiScanList;

    private String networkSSID = "";

    private boolean backgroundWifiScan = true;


    private String hostIP = "";

    WifiManager wifi;
    WifiScanReceiver wifiReciever;

    LinearLayout wifiLayout, passworLayout;



    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_unit);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        unitsSpinner = (Spinner)findViewById(R.id.unitsSpinner);

        homeNetworkSpinner=(Spinner)findViewById(R.id.homeNetworkSpinner);

        passwordText = (EditText) findViewById(R.id.passwordText);
        passwordText.setText("");

        messageLabel = (TextView) findViewById(R.id.messageLabel);
        homeNetworkLabel = (TextView) findViewById(R.id.homeNetworkLabel);
        unitLabel = (TextView) findViewById(R.id.unitLabel);
        detectWifisLabel = (TextView) findViewById(R.id.detectWifisLabel);
        detectUnitsLabel = (TextView) findViewById(R.id.detectUnitsLabel);

        /*
        wifiLayout = (LinearLayout) findViewById(R.id.wifiLayout);
        passworLayout = (LinearLayout) findViewById(R.id.passwordLayout);
        wifiLayout.setAlpha((float)0);
        passworLayout.setAlpha((float)0);
        wifiLayout.setEnabled(false);
        passworLayout.setEnabled(false);
        */




        messageLabel.setText("Connect your unit");
        homeNetworkLabel.setText("Please set your WIFI data");
        unitLabel.setText("Detected unit");


        // show nbr of current user units
        Intent intent = getIntent();
        currentNbrUnits = intent.getExtras().getLong(MainActivity.NBR_UNITS);

        refreshButton = (ImageButton) findViewById(R.id.refreshButton);
        submitButton = (Button) findViewById(R.id.submitButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);

        //requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
        wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
        Log.d("MyLog", "Start wifi manager");
        wifiReciever = new WifiScanReceiver();
        wifi.startScan();


        //  forget humidex units wifi
        forgetHumidexUnitsWifi();



        // refresh button action listener
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroundWifiScan = true;
                loadingProgress = new ProgressDialog(AddUnitActivity.this);
                loadingProgress.setMessage("Scanning WiFi...");
                loadingProgress.show();
                forgetHumidexUnitsWifi();
                wifi.startScan();
            }
        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(unitsSpinner.getSelectedItem()==null)
                    alert("No unit detected, please click refresh, select a unit and try again.");
                else
                {
                    String selectSSID = unitsSpinner.getSelectedItem().toString().replaceAll("\\s", "");
                    // if the device is already connected
                    if (true) {
                        // if wifi is enabled

                        if (wifi.isWifiEnabled()) {
                            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                            WifiInfo info = wifi.getConnectionInfo();
                            String ssid = info.getSSID();
                            ssid = ssid.replaceAll("\\s", "");
                            ssid = ssid.replaceAll("\"", "");

                            if (mWifi.isConnected() && ssid.equals(selectSSID)) {
                                // Do whatever
                                //alert("You are currently connected to the same SSID: " + ssid);
                                // get ip address
                                hostIP = getApIpAddr(AddUnitActivity.this);

                                // dismiss progress bar
                                if (loadingProgress != null)
                                    loadingProgress.dismiss();

                                // change connected status
                                deviceConnected = true;

                                //alert("You are currently connected to the same SSID.\nSSID: " + networkSSID + "\nHost IP: " + hostIP);
                                submitData();

                            } else {

                                loadingProgress = new ProgressDialog(AddUnitActivity.this);
                                loadingProgress.setMessage("Connecting...");
                                loadingProgress.show();

                                if (connectToAP(selectSSID, defaultPassword) != -1) {
                                    // you are connected
                                    loadingProgress.setMessage("Connected\nSending data to the unit...");

                                    // Execute some code after 4 seconds have passed. This used to wait until the device get connected to be able to get the host IP address
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            // After 4 seconds check if the device is connected to a Wifi connection or not
                                            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                                            if (mWifi.isConnected()) {
                                                // get ip address
                                                hostIP = getApIpAddr(AddUnitActivity.this);

                                                // dismiss progress bar
                                                if (loadingProgress != null)
                                                    loadingProgress.dismiss();

                                                // change connected status
                                                deviceConnected = true;

                                                //alert("You are connected.\nSSID: " + networkSSID + "\nHost IP: " + hostIP);

                                                submitData();
                                            } else {
                                                // dismiss progress bar
                                                if (loadingProgress != null)
                                                    loadingProgress.dismiss();
                                                // an error occured while trying to connect
                                                alert("An error occured. Please make sure that you select a Humidex brand unit and try again.");
                                            }

                                        }
                                    }, 4000);


                                } else {
                                    // dismiss progress bar
                                    if (loadingProgress != null)
                                        loadingProgress.dismiss();
                                    // an error occured while trying to connect
                                    alert("An error occurred. Please try again.");
                                }

                            }
                        } else {
                            // wifi is disabled
                            alert("Your WiFi is disabled. Please enable WiFi and try again.");
                        }

                }



                }
            }
        });

    }


    // forget humidex units wifi
    private void forgetHumidexUnitsWifi() {
        if (wifi.isWifiEnabled()) {
            System.out.println("Device connected");
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration i : list) {
                String ssid = wifiManager.getConnectionInfo().getSSID();
                if(ssid.contains(filterText)){
                    System.out.println("Forget network "+ssid);
                    wifiManager.removeNetwork(i.networkId);
                    wifiManager.saveConfiguration();
                }

            }
        }
        else{
            System.out.println("Device not connected");
            Tools.alert("No network connection", "Your device Wi-Fi is disabled, please disable your Wi-Fi and try again.",this);

            }

        }




    // submit data to the unit
    public void submitData(){
        if(!deviceConnected){
            alert("You have to be connected to the unit access point before you proceed.");
        }
        else if(hostIP.isEmpty()){
            alert("Enable to get host IP address. Please press 'Reset' button and try again.");

        }
        else
        {
            // check data entered by the user
            if(homeNetworkSpinner.getSelectedItem()==null){
                alert("Please select your home network");
            }
            else if (passwordText.getText().toString().isEmpty()){
                passwordText.setError("Please enter your home network password");
            }
            else
            {
                // everything is good
                homeWifiSSID = homeNetworkSpinner.getSelectedItem().toString().replaceAll("\\s", "");
                homePassword = passwordText.getText().toString();
                String userdID = LoginSignUpActivity.userId;
                String url ="http://"+hostIP+"/?ssid="+homeWifiSSID+"&password="+homePassword+"&userid="+userdID;
                System.out.println("Send request to: "+url);
                sendHttpRequest(url);
            }
        }

    }







    protected void onPause() {
        unregisterReceiver(wifiReciever);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }









    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        backgroundWifiScan = false;
        try{
            ((TextView) homeNetworkSpinner.getSelectedView()).setTextColor(Color.BLUE);
            ((TextView) homeNetworkSpinner.getSelectedView()).setTextSize(16);
            if(unitsSpinner.getSelectedView()!=null){
                ((TextView) unitsSpinner.getSelectedView()).setTextColor(Color.BLUE);
                ((TextView) unitsSpinner.getSelectedView()).setTextSize(16);
            }

        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }








    public void connectToWifi(String ssid, String passkey) {
        ssid = ssid.replaceAll("\\s","");
        System.out.println("Wifi SSID: "+"\""+ssid+"\""+"\t Password: "+"\""+defaultPassword+"\"");
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        // setup a wifi configuration
        WifiConfiguration wc = new WifiConfiguration();
        wc.SSID = "\""+ssid+"\"";
        wc.preSharedKey = "\""+defaultPassword+"\"";
        wc.status = WifiConfiguration.Status.ENABLED;
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        // connect to and enable the connection
        int netId = wifiManager.addNetwork(wc);
        wifiManager.enableNetwork(netId, true);
        wifiManager.setWifiEnabled(true);

        System.out.println("Wifi getConnection INFO: "+wifiManager.getConnectionInfo());
        if(loadingProgress!=null)
            loadingProgress.dismiss();
        alert("You are connected to "+ssid);
        getApIpAddr(this);
    }











    public String getApIpAddr(Context context) {
        //WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifi.getDhcpInfo();
        byte[] ipAddress = convert2Bytes(dhcpInfo.serverAddress);
        try {
            String hostIpAddress = InetAddress.getByAddress(ipAddress).getHostAddress();
            System.out.println("IP Address: "+hostIpAddress);
            return hostIpAddress;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static byte[] convert2Bytes(int hostAddress) {
        byte[] addressBytes = { (byte)(0xff & hostAddress),
                (byte)(0xff & (hostAddress >> 8)),
                (byte)(0xff & (hostAddress >> 16)),
                (byte)(0xff & (hostAddress >> 24)) };
        return addressBytes;
    }









    public int connectToAP(String networkSSID, String networkPasskey) {
        try{
            for (ScanResult result : wifiScanList) {

                if (result.SSID.equals(networkSSID)) {
                    System.out.println("Network match found: "+networkSSID+" Password: "+networkPasskey);
                    String securityMode = getScanResultSecurity(result);
                    System.out.println("Security mode: "+securityMode);

                    WifiConfiguration wifiConfiguration = createAPConfiguration(networkSSID, networkPasskey, securityMode);

                    int res = wifi.addNetwork(wifiConfiguration);
                    Log.d(TAG, "# addNetwork returned " + res);

                    boolean b = wifi.enableNetwork(res, true);
                    Log.d(TAG, "# enableNetwork returned " + b);

                    wifi.setWifiEnabled(true);

                    boolean changeHappen = wifi.saveConfiguration();

                    if (res != -1 && changeHappen) {
                        Log.d(TAG, "# Change happen: SSID name: "+networkSSID);
                        this.networkSSID = networkSSID;
                    } else {
                        Log.d(TAG, "# Change NOT happen");
                    }

                    return res;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        return -1;
    }









    public String getScanResultSecurity(ScanResult scanResult) {
        Log.i(TAG, "* getScanResultSecurity");

        final String cap = scanResult.capabilities;
        System.out.println("CAP: "+cap);
        final String[] securityModes = { "WEP", "PSK", "EAP", "WAP2" };

        for (int i = securityModes.length - 1; i >= 0; i--) {
            if (cap.contains(securityModes[i])) {
                return securityModes[i];
            }
        }

        return "OPEN";
    }









    public void alert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });

        // Create the AlertDialog object
        builder.create();
        builder.show();
    }












    private WifiConfiguration createAPConfiguration(String networkSSID, String networkPasskey, String securityMode) {
        WifiConfiguration wifiConfiguration = new WifiConfiguration();

        wifiConfiguration.SSID = "\"" + networkSSID + "\"";

        if (securityMode.equalsIgnoreCase("OPEN")) {

            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        } else if (securityMode.equalsIgnoreCase("WEP")) {

            wifiConfiguration.wepKeys[0] = "\"" + networkPasskey + "\"";
            wifiConfiguration.wepTxKeyIndex = 0;
            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);

        } else if (securityMode.equalsIgnoreCase("PSK")) {

            wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

            wifiConfiguration.preSharedKey = "\"".concat(networkPasskey).concat("\"");

        } else {
            Log.i(TAG, "# Unsupported security mode: "+securityMode);
            alert("Error occurred while trying to connect the unit. Unsupported security mode");
            return null;
        }

        return wifiConfiguration;

    }










    public void sendHttpRequest(String url){
        try{
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);


            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Display the first 500 characters of the response string.
                            System.out.println("Response is: "+ response);

                            // if unit sends "1" => everything is good
                            if(response.contains("1")){

                                loadingProgress = new ProgressDialog(AddUnitActivity.this);
                                loadingProgress.setMessage("Unit connected successfully\nReconnecting...");
                                loadingProgress.show();

                                System.out.println("homeWifiSSID:"+homeWifiSSID+" \tpassword:"+homePassword);
                                wifi.disconnect();

                                reconnected = false;


                                if(connectToAP(homeWifiSSID.replaceAll("\\s", ""), homePassword)==-1)
                                {
                                    if(loadingProgress!=null)
                                        loadingProgress.dismiss();

                                    AlertDialog.Builder builder = new AlertDialog.Builder(AddUnitActivity.this);
                                    builder.setMessage("An error occurred while trying to reconnect. Please try to reconnect to your home network.")
                                            .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    finish();

                                                }
                                            });

                                    // Create the AlertDialog object
                                    builder.create();
                                    builder.show();


                                    reconnected = false;

                                }


                                else
                                {
                                    reconnected = true;
                                    loadingProgress.setMessage("User connected, scanning for new added unit...");
                                }



                                // Execute some code after 4 seconds have passed. This used to wait until the device reconnect to the home network
                                handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(reconnected)
                                            scanUserUnits();
                                        else
                                            alert("Scan failed. An error occurred while trying to reconnect. Please try to reconnect to your home network manually and check if the unit added successfully in the main interface.");

                                        if(loadingProgress!=null)
                                            loadingProgress.dismiss();
                                        //finish();

                                    }
                                }, 5000);

                            }else{
                                alert("Failed to setup the unit please try again.\nUnit answer: Fail.");
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("An error occurred while trying to send HTTP request");
                }
            });
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        }catch (Exception e){
            alert("An error occurred while trying to send HTTP request.");
        }

    }

    public void scanUserUnits(){
        System.out.println("Scanning current user units:  EMAIL: "+ LoginSignUpActivity.authUser.getEmail()+" ID: "+ LoginSignUpActivity.authUser.getUid());

        // create a reference to Firebase database
        DatabaseReference myDBRootRef = FirebaseDatabase.getInstance().getReference();
        // create a reference to Firebase database 'users' table
        DatabaseReference myUsersUnitsRef = myDBRootRef.child("users_units").getRef();

        myUsersUnitsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // print number of records in 'users' table
                Log.e("Count " ,""+dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).getChildrenCount());
                long nbrUnits = dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).getChildrenCount();
                if(nbrUnits==currentNbrUnits) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(AddUnitActivity.this);
                    builder.setMessage("The new unit successfully registered to our database.")
                            .setPositiveButton("Back", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();

                                }
                            });
                    builder.setNegativeButton("View units",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(AddUnitActivity.this, ListUnitActivity.class));

                        }
                    });

                    // Create the AlertDialog object
                    builder.create();
                    builder.show();
                }
                else
                    alert("Error: The registration process failed.");


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {


            if(backgroundWifiScan)
            {
                detectWifisLabel.setText("Loading...");
                detectUnitsLabel.setText("Loading...");
                wifiScanList = wifi.getScanResults();
                //wifis = new String[wifiScanList.size()];
                List<String> wifis = new ArrayList<String>();
                List<String> myHomeWifis = new ArrayList<String>();
                //wifis.add("Select...");
                for(int i = 0; i < wifiScanList.size(); i++){
                    String tempWifiName = wifiScanList.get(i).toString().substring(wifiScanList.get(i).toString().indexOf(" "),wifiScanList.get(i).toString().indexOf(","));
                    tempWifiName.replaceAll(" ", "");
                    if(tempWifiName.length()>1)
                        if(tempWifiName.contains(filterText))
                            wifis.add(tempWifiName);
                        else
                        if(!myHomeWifis.contains((String) tempWifiName))
                            myHomeWifis.add(tempWifiName);

                    Log.d("MyLog", (tempWifiName));
                }

                if(wifis.size()==0)
                    detectUnitsLabel.setText("No Unit");
                else if(wifis.size()==1)
                    detectUnitsLabel.setText("1 Unit");
                else
                    detectUnitsLabel.setText(wifis.size()+" Units");

                if(myHomeWifis.size()==0)
                    detectWifisLabel.setText("No WiFi");
                else if(myHomeWifis.size()==1)
                    detectWifisLabel.setText("1 WiFi");
                else
                    detectWifisLabel.setText(myHomeWifis.size()+" WiFis");

                // Creating adapter for spinner
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddUnitActivity.this, android.R.layout.simple_spinner_item, wifis);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                unitsSpinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,wifis));
                unitsSpinner.setOnItemSelectedListener(AddUnitActivity.this);

                // Creating adapter for spinner
                dataAdapter = new ArrayAdapter<String>(AddUnitActivity.this, android.R.layout.simple_spinner_item, myHomeWifis);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                homeNetworkSpinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,myHomeWifis));
                homeNetworkSpinner.setOnItemSelectedListener(AddUnitActivity.this);
                if(loadingProgress!=null)
                    loadingProgress.dismiss();
            }

        }

    }




}
