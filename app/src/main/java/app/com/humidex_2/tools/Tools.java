package app.com.humidex_2.tools;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;

/**
 * Created by Fahmi on 2016-10-28.
 */

public class Tools {

    /**
     * show a simple alert dialog
     * @param message
     * @param okBtnText
     * @param context
     */
    public static void alert(String title, String message, String okBtnText, Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message)
                .setPositiveButton(okBtnText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });

        // Create the AlertDialog object
        builder.create();
        builder.show();

    }

    /**
     * show a simple alert dialog
     * @param title
     * @param message
     * @param context
     */
    public static void alert(String title, String message, Context context){
        alert(title, message, "OK", context);
    }


    /**
     * show a simple alert dialog
     * @param message
     * @param context
     */
    public static void alert(String message, Context context){
       alert("Message", message, "OK", context);
    }



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean checkConnection(Context context) {

        boolean mobileDataAllowed = android.provider.Settings.Global.getInt(context.getContentResolver(), "mobile_data", 1) == 1;
        System.out.println("Mobile data: "+android.provider.Settings.Global.getInt(context.getContentResolver(), "mobile_data", 1));
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        WifiInfo info = wifiManager.getConnectionInfo();
        String ssid = info.getSSID();
        ssid = ssid.replaceAll("\\s", "");
        ssid = ssid.replaceAll("\"", "");
        if (mWifi.isConnected() || mobileDataAllowed) {
            return true;
        } else
            return false;

    }



    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
