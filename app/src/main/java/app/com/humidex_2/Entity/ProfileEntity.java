package app.com.humidex_2.Entity;

/**
 * Created by Daniel-PC on 10/19/2016.
 */

public class ProfileEntity {

    private String first_name;
    private String last_name;
    private String city;
    private String address;
    private String areaCode;
    private String phone_number;

    public ProfileEntity(){
        this.first_name = "";
        this.last_name = "";
        this.city = "";
        this.address = "";
        this.areaCode = "";
        this.phone_number = "";
    }

    public ProfileEntity(String first_name1, String last_name1, String city1, String address1, String areaCode1, String phone_number1){
        this.first_name = first_name1;
        this.last_name = last_name1;
        this.city = city1;
        this.address = address1;
        this.areaCode = areaCode1;
        this.phone_number = phone_number1;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
