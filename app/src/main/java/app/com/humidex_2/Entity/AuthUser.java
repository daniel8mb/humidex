package app.com.humidex_2.Entity;

/**
 * Created by Fahmi on 2016-10-26.
 */

public class AuthUser {
    String email;
    String uid;
    public AuthUser(String email, String uid){
        this.email = email;
        this.uid = uid;
    }

    public String getEmail(){
        return this.email;
    }


    public String getUid(){
        return this.uid;
    }
}
