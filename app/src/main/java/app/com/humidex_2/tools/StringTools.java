package app.com.humidex_2.tools;

import android.text.TextUtils;
import android.widget.EditText;

/**
 * Created by Fahmi on 2016-10-28.
 */

public class StringTools {
    /**
     * Check passwords
     * @param password
     * @param passwordRepeat
     * @param mPasswordRepeat
     * @return
     */
    public static boolean passwordPasswordRepeat(String password, String passwordRepeat, EditText mPasswordRepeat){

        boolean info = false;

        if (!passwordRepeat.equals(password)){
            mPasswordRepeat.setError("Password and PasswordRepeat are not same");
            info =  false;
        }
        else{
            info = true;
        }
        return info;
    }

    /**
     * Verification Email et password
     * */

    /**
     *
     * @param email
     * @param password
     * @param mEmail
     * @param mPassword
     * @return
     */
    public static boolean verifEmailPassowrd(String email, String password, EditText mEmail, EditText mPassword){

        boolean info = true;

        // email shouldn't be empty
        if (TextUtils.isEmpty(email)) {
            mEmail.setError("Enter email address!");
            info = false;
        }
        // password shouldn't be empty
        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Enter password!");
            info = false;
        }

        // password lenght should be more than 6
        if (password.length() < 6) {
            mPassword.setError("Password too short, enter minimum 6 characters!");
            info = false;
        }

        return info;
    }

}
