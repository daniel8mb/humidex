package app.com.humidex_2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

import app.com.humidex_2.Entity.AuthUser;
import app.com.humidex_2.tools.StringTools;
import app.com.humidex_2.tools.Tools;

/**
 * Created by Daniel-PC on 10/18/2016.
 */

public class LoginSignUpActivity extends Activity {

    public boolean signInSignup = true;

    public static AuthUser authUser;
    private EditText signUpEmail;
    private EditText signUpPass;
    private EditText signUpPassRepeat;

    private EditText signInEmail;
    private EditText signInPass;

    private Button btnRegister;//display view for Sign Up
    private Button btnSignIn;//send data for Sign In
    private Button btnSignUp;//Send data for Sign Up

    private FirebaseUser user;//Le User

    static String userId;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    ProgressDialog loadingProgress;

    private static final String TAG = "EmailPassword";
    public static final String USER_EMAIL_EXTRA = "app.com.humidex_2.LoginSignUpActivity.USER_EXTRA";
    public static final String USER_PASS_EXTRA = "app.com.humidex_2.LoginSignUpActivity.PASS_EXTRA";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_sign_up);

        // init interface
        initComponents();


        // check Firebase user
        checkUser();

        // init buttons action
        initButtonAction();


    }




    public void checkUser() {
        // get Auth instance
        mAuth = FirebaseAuth.getInstance();
        // get existing firebase user
        user = mAuth.getCurrentUser();
        // if the a user found, fire an alert to the user to tell him that his session is still active
        if (user != null) {
            System.out.println("User logged in: " + mAuth.getCurrentUser().getEmail());
            // start main activity
            authUser = new AuthUser(mAuth.getCurrentUser().getEmail(), mAuth.getCurrentUser().getUid());
            userId = user.getUid();
            Intent intent = new Intent(LoginSignUpActivity.this, MainActivity.class);
            startActivity(intent);
            finish();

            /*
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Your session still active, would like to proceed to your profile?")
                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // start main activity
                            authUser = new AuthUser(mAuth.getCurrentUser().getEmail(), mAuth.getCurrentUser().getUid());
                            userId = user.getUid();
                            Intent intent = new Intent(LoginSignUpActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
            builder.setNegativeButton("Logout", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // sign out
                    mAuth.signOut();
                }
            });
            // Create the AlertDialog object
            builder.create();
            builder.show();
            */

        }
    }



    /**
     * Init graphical components
     */
    public void initComponents(){
        signUpEmail = (EditText) findViewById(R.id.signUpEmail);
        signUpPass = (EditText) findViewById(R.id.signUpPass);
        signUpPassRepeat = (EditText) findViewById(R.id.signUpPassRepeat);

        signInEmail = (EditText) findViewById(R.id.email);
        signInPass = (EditText) findViewById(R.id.pass);

        btnRegister = (Button) findViewById(R.id.btnRegister);

        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
    }


    /**
     * Initalisation des actions des boutons
     * */

    public void initButtonAction(){
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = signInEmail.getText().toString();
                String password = signInPass.getText().toString();

                if (StringTools.verifEmailPassowrd(email, password, signInEmail, signInPass))
                        signInAction(email, password);

            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = signUpEmail.getText().toString();
                String password = signUpPass.getText().toString();
                String passwordRepeat = signUpPassRepeat.getText().toString();

                if (StringTools.verifEmailPassowrd(email, password, signUpEmail, signUpPass)){
                    if (StringTools.passwordPasswordRepeat(password, passwordRepeat, signUpPassRepeat))
                        signUpAction(email, password);
                }
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!signInSignup){
                    displaySignIn();
                    btnRegister.setText("Sign Up");
                    signInSignup = true;
                }
                else{
                    dispalySignUp();
                    signInSignup = false;
                    btnRegister.setText("Sign In");
                }


            }
        });
    }

    /**
     * Display Views for Sign In
     * */

    public void displaySignIn(){
        signInEmail.setVisibility(View.VISIBLE);
        signInPass.setVisibility(View.VISIBLE);
        btnSignIn.setVisibility(View.VISIBLE);

        signUpEmail.setVisibility(View.GONE);
        signUpPass.setVisibility(View.GONE);
        signUpPassRepeat.setVisibility(View.GONE);
        btnSignUp.setVisibility(View.GONE);
    }

    /**
     * Display views for Sign Up
     * */

    public void dispalySignUp(){
        signInEmail.setVisibility(View.GONE);
        signInPass.setVisibility(View.GONE);
        btnSignIn.setVisibility(View.GONE);

        signUpEmail.setVisibility(View.VISIBLE);
        signUpPass.setVisibility(View.VISIBLE);
        signUpPassRepeat.setVisibility(View.VISIBLE);
        btnSignUp.setVisibility(View.VISIBLE);
    }


    /**
     * sing up action
     * @param email
     * @param password
     */
    public void signUpAction(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if(task.isSuccessful()){
                            authUser = new AuthUser(mAuth.getCurrentUser().getEmail(), mAuth.getCurrentUser().getUid()) ;
                            Intent intent = new Intent(LoginSignUpActivity.this, ProfileActivity.class);
                            intent.putExtra("REGISTER_UPDATE", 1);
                            startActivity(intent);
                            finish();
                        }

                        if (!task.isSuccessful()) {
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                signInPass.setError(e.getMessage());
                                signInPass.requestFocus();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                signInEmail.setError(e.getMessage());
                                signInEmail.requestFocus();
                            } catch(FirebaseAuthUserCollisionException e) {
                                signInEmail.setError(e.getMessage());
                                signInEmail.requestFocus();
                            }catch (FirebaseNetworkException e){
                                e.printStackTrace();
                                Tools.alert(e.getMessage()+"\nPlease check your network connection and try again.", LoginSignUpActivity.this);
                            } catch(Exception e) {
                                Log.e(TAG, e.getMessage());
                                Tools.alert(e.getMessage(), LoginSignUpActivity.this);
                            }
                        }

                    }
                });

    }






    /**
     * sign in action
     * @param email
     * @param password
     */
    public void signInAction(String email, String password) {

        loadingProgress = new ProgressDialog(this);
        loadingProgress.setMessage("Loading, please wait...");
        loadingProgress.show();

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.


                                task.isSuccessful();
                                if (task.isSuccessful()){
                                    authUser = new AuthUser(mAuth.getCurrentUser().getEmail(), mAuth.getCurrentUser().getUid());
                                    Intent intent = new Intent(LoginSignUpActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    if(loadingProgress!=null)
                                        loadingProgress.dismiss();

                                    finish();
                                }

                            if(!task.isSuccessful()) {
                                if(loadingProgress!=null)
                                    loadingProgress.dismiss();
                                try {
                                    throw task.getException();
                                } catch(FirebaseAuthWeakPasswordException e) {
                                    signInPass.setError(e.getMessage());
                                    signInPass.requestFocus();
                                } catch(FirebaseAuthInvalidCredentialsException e) {
                                    signInEmail.setError(e.getMessage());
                                    signInEmail.requestFocus();
                                } catch(FirebaseAuthUserCollisionException e) {
                                    signInEmail.setError(e.getMessage());
                                    signInEmail.requestFocus();
                                }catch (FirebaseNetworkException e){
                                    e.printStackTrace();
                                    Tools.alert(e.getMessage()+"\nPlease check your network connection and try again.", LoginSignUpActivity.this);
                                } catch(Exception e) {
                                    Log.e(TAG, e.getMessage());
                                    Tools.alert(e.getMessage(), LoginSignUpActivity.this);
                                }
                            }



                            // ...
                        }
                    });


    }






}
