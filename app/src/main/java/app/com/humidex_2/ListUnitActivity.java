package app.com.humidex_2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.com.humidex_2.Addon.Pref;
import app.com.humidex_2.Entity.UnitData;
import app.com.humidex_2.tools.Tools;

public class ListUnitActivity extends AppCompatActivity {
    ProgressDialog loadingProgress;

    private ListView listViewUnit;//Affichera la liste des unitées
    private ArrayList<UnitData> listData;//Liste des unitées
    private RecyclerView recyclerView;
    private List<HashMap<String, String>> listContent;//liste de certaines donnees (comme l'Id et le nom) provenant des unites
    private Intent intent;
    private ListAdapter adapter;//adapter pour la listView listUnit
    public static UnitData mUnit;
    public final String[] months = {"Jan.", "Feb.", "Mar.", "Apr.", "Mai.", "Jun.", "Jul.", "Aug.","Sep.", "Oct.", "Nov.", "Dec."};

    public static String selectedUnitName = "";

    private List listKey, unitsNameList;

    // create a reference to Firebase database
    DatabaseReference myDBRootRef = FirebaseDatabase.getInstance().getReference();
    // create a reference to Firebase database 'users' table
    DatabaseReference myUsersUnitsRef = myDBRootRef.child("users_units").getRef();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_unit);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listKey = new ArrayList();
        unitsNameList = new ArrayList();

        intent = getIntent();
        scanUserUnits();
        loadingProgress = new ProgressDialog(this);
        loadingProgress.setMessage("Scanning user units...");
        loadingProgress.show();



        //listViewUnit = (ListView) findViewById(R.id.list_unit);
        recyclerView = (RecyclerView) findViewById(R.id.list_unit);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        listData = new ArrayList<UnitData>();
        listContent = new ArrayList<>();

        // Execute some code after 4 seconds have passed. This used to wait until the device get connected to be able to get the host IP address
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(loadingProgress!=null)
                    loadingProgress.dismiss();
                initListData();
            }
        }, 1000);




    }
    /**
     * Initialisation de la listView
     * */
    public void initListData(){

        MyRecyclerAdapter adapter = new MyRecyclerAdapter(getApplicationContext(), unitsNameList);
        recyclerView.setAdapter(adapter);

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, unitsNameList);

        /listViewUnit.setAdapter(adapter);
        listViewUnit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedUnitName = unitsNameList.get(i).toString().split("\\(")[0].replaceAll("\\s", "");
                Intent intent1 = new Intent(ListUnitActivity.this, UnitDataActivity.class);
                intent1.putExtra(Pref.UNITDATA, listKey.get(i).toString());
                startActivity(intent1);
            }
        });*/
    }




    public void scanUserUnits(){
        System.out.println("Scanning current user units:  EMAIL: "+LoginSignUpActivity.authUser.getEmail()+" ID: "+LoginSignUpActivity.authUser.getUid());

        if(loadingProgress!=null)
            loadingProgress.dismiss();

        myUsersUnitsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    // print number of records in 'users' table
                    Log.e("Count " ,""+dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).getChildrenCount());
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.child(LoginSignUpActivity.authUser.getUid()).getChildren()) {
                        listKey.add(dataSnapshot1.getKey());
                        String unitName = (String) dataSnapshot1.child("name").getValue();
                        String timestamp = (String) dataSnapshot1.child("timestamp").getValue();
                        if(timestamp!=null &&  unitName!=null){
                            String date[] = timestamp.split("_");
                            unitsNameList.add(unitName+" (added "+months[Integer.parseInt(date[1])-1]+" "+date[2]+", "+date[0]+")");
                        }
                        else
                        {
                            unitsNameList.add("Unknown Unit");
                        }




                        System.out.println("Unit id: " + unitName);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Tools.alert("An error occurred, missing unit's information.", ListUnitActivity.this);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder>{

        private List mListDataset;
        private Context mContext;

        public MyRecyclerAdapter(Context context, List listData){
            this.mListDataset = listData;
            this.mContext = context;
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            private TextView mText1;
            private ImageView imageView;
            private 	android.support.v7.widget.CardView cardView;
            //public CardView cv;

            public ViewHolder(View itemView){
                super(itemView);

                cardView = (CardView) itemView.findViewById(R.id.item_cv);
                mText1 = (TextView) itemView.findViewById(R.id.item_text1);
                imageView = (ImageView) itemView.findViewById(R.id.item_image);

                //imageView.getLayoutParams().width = 400;
                //imageView.getLayoutParams().height = 400;

                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectedUnitName = mListDataset.get(getLayoutPosition()).toString().split("\\(")[0].replaceAll("\\s", "");
                        Intent intent1 = new Intent(ListUnitActivity.this, UnitDataActivity.class);
                        intent1.putExtra(Pref.UNITDATA, listKey.get(getLayoutPosition()).toString());
                        startActivity(intent1);
                    }
                });
            }
        }

        @Override
        public int getItemCount(){
            return mListDataset.size();
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mText1.setText("" + mListDataset.get(position));
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_even_list_layout2, parent, false);
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }
    }

}
