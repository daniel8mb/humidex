package app.com.humidex_2.Addon;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.com.humidex_2.Entity.UnitData;
import app.com.humidex_2.Entity.User;

/**
 * Created by Daniel-PC on 10/20/2016.
 */

public class Pref {

    public static final String PREFS_NAME = "preference_name";
    public static final String PREF_FIRST_NAME = "first_name";
    public static final String PREF_LAST_NAME = "last_name";
    public static final String PREF_ADRRESS = "address";
    public static final String PREF_AREA_CODE = "area_code";
    public static final String PREF_PHONE_NUMBER = "phone_number";
    public static final String PREF_CITY = "city";
    public static final String PREF_MY_USER = "User";

    public static final String LISTDATA = "MainActivity.LISTDATA";
    public static final String UNITDATA = "ListUnitActivity.UNITDATA";
    public static final String UNITDATAKEY = "unitDataKey";
    public static final String UNITMAXFUNSPEED = "maxfunspeed";
    public static final String UNITDESIREDHUMIDITY = "desiredhumidity";

    public static List listUnitKey = new ArrayList();
    public static List listUnitName = new ArrayList();
    public static ArrayList<UnitData> listUnitData = new ArrayList<UnitData>();
    public static UnitData mUnit = new UnitData();


}
